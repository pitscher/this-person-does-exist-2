# This Person Does Exist
Is a little project that displays the FFHQ dataset in a random fashion like thispersondoesnotexist.com

# The Project
The app loads the ffhq-dataset-v2.json file and serves a random object from it over an express API.
The client then sorts and displays all necessary data.


# Made with Glitch
This app is running on glitch.com which I am very happy about :) 
Glitch is the friendly community where you'll build the app of your dreams. Glitch lets you instantly create, remix, edit, and host an app, bot or site, and you can invite collaborators or helpers to simultaneously edit code with you.

Find out more about Glitch.

( ᵔ ᴥ ᵔ )